Bootstrap: docker
From: nvidia/cuda:10.2-devel-ubuntu18.04

%labels
  Author Julien Seiler <seilerj@igbmc.fr>

%post
	# Install system dependencies
	apt-get update
	apt-get install -y wget libgtk2.0-0 libxxf86vm1 libgl1
  rm -rf /var/lib/apt/lists/*

	# Download and install minconda
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
  chmod +x Miniconda3-latest-Linux-x86_64.sh
  ./Miniconda3-latest-Linux-x86_64.sh -b -s -p /opt/conda
  rm Miniconda3-latest-Linux-x86_64.sh
  export PATH=/opt/conda/bin:$PATH

  # Install cryolo
  conda install -y -c conda-forge -c anaconda python=3.6 pyqt=5 cudnn=7.1.2 numpy==1.14.5 cython wxPython==4.0.4 intel-openmp==2019.4
  pip install 'cryolo[gpu]==1.7.4'
  conda clean -a -y

%environment
  export PATH=/opt/conda/bin:$PATH

%test
  export PATH=/opt/conda/bin:$PATH
  which cryolo_boxmanager.py
  which cryolo_evaluation.py
  which cryolo_gui.py
  which cryolo_predict.py
  which cryolo_train.py
  cryolo_train.py --help
